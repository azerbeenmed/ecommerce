<?php
// src/Controller/AdmincontController.php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Doctrine\ORM\EntityManagerInterface;

class AdmincontController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/admin', name: 'app_admin')]
    public function index(): Response
    {
        // Retrieve all users from the database
        $userRepository = $this->entityManager->getRepository(User::class);
        $users = $userRepository->findAll();

        // Render the template with the users
        return $this->render('admin/index.html.twig', [
            'users' => $users,
        ]);
    }
}
