<?php

namespace App\Controller;

use App\Entity\Categories;
use App\Repository\SubCategoriesRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SubcategoriesController extends AbstractController
{
    #[Route('/subcategories', name: 'app_sub_categories')]

    public function index(SubCategoriesRepository $SubCategoriesRepository): Response
    {          $SubCategories = $SubCategoriesRepository->findAll();

        return $this->render('subcategories/index.html.twig', [
            'SubCategories' => $SubCategories,
        ]);
    }
    #[Route('/subcategories/{id}', name: 'app_sub_cat_child')]
    public function child( Categories $category): Response
    {
        // Assuming you have a method named 'getSubCategories' in your Categories entity
        $subCategories = $category->getSubCategories();

        return $this->render('subcategories/index.html.twig', [
            'SubCategories' => $subCategories,
        ]);
    }}