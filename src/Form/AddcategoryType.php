<?php

namespace App\Form;

use App\Entity\Categories;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddcategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name',TextType::class,[
                'attr' =>[
                     'class'=>'form-control'
                ],
                'label' => 'Nom'
            ])
            ->add('image', Filetype::class,[
                'attr' =>[
                     'class'=>'form-control'
                ],
                'label' => 'image'
            ])
            ->add('slug', TextType::class,[
                'attr' =>[
                    'class'=>'form-control'
                ],
                'label' => 'name'
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Categories::class,
        ]);
    }
}
